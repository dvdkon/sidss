import preprocess from "svelte-preprocess";
import { optimizeImports } from "carbon-preprocess-svelte";
import staticAdapter from "@sveltejs/adapter-static";

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: [
        preprocess({
            scss: {
                prependData: '@use "carbon-components/scss/globals/scss/_theme-tokens" as *; @use "carbon-components/scss/globals/scss/vendor/@carbon/elements/scss/type/_styles.scss" as *;',
            },
            typescript: {},
        }),
        optimizeImports()
    ],

	kit: {
		// hydrate the <div id="svelte"> element in src/app.html
        target: '#svelte',
        prerender: {
            enabled: false
        },
        ssr: false,
        adapter: staticAdapter({
            fallback: "index.html",
        }),
    }
};

export default config;
