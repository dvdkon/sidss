import type { Writable } from "svelte/store";
import { writable, derived } from "svelte/store";
import { getGroups, getSelfInfo, getServices, login as loginEndpoint } from "sidss-common/httpApi";
import type { Service, Group, User } from "sidss-common/mgmtModel";
import { callEndpoint } from "./api";
import { SERVICE_NAME } from "./config";

function localStorageWritable<T>(key: string, defaultValue: T): Writable<T> {
    let storedString: string = null;
    if(typeof window !== "undefined") {
        storedString = window.localStorage.getItem(key);
    }
    if(storedString) {
        defaultValue = JSON.parse(storedString);
    }
    const store = writable<T>(defaultValue);
    if(typeof window !== "undefined") {
        store.subscribe(newValue =>
            window.localStorage.setItem(key, JSON.stringify(newValue)));
    }
    return store;
}

export const token = localStorageWritable<string>("token", null);

export const currentUser = writable<{
    id: string,
    user: User,
    fullPermissions: string[],
}>(null);
export const services = writable<{id: string, service: Service}[]>([]);
export const groups = writable<{id: string, group: Group}[]>([]);

export const isAdmin = derived(currentUser, u =>
    u && u.fullPermissions.includes(SERVICE_NAME + ".admin"));

export async function loadGlobalData() {
    currentUser.set(await callEndpoint(getSelfInfo, undefined));
    services.set(await callEndpoint(getServices, undefined));
    groups.set(await callEndpoint(getGroups, undefined));
}

export async function login(username: string, password: string) {
    const {token: tokenVal} = await callEndpoint(loginEndpoint, {username, password});
    token.set(tokenVal);

    await loadGlobalData();
}
