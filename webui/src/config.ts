// All global code accessing `window` (and its children) needs to not crash
// work without it due to SSR and this issue:
// https://github.com/sveltejs/kit/issues/1650
export const API_BASE = "http://" + (typeof location !== "undefined" ? location.hostname : "") + ":7010";
export const SERVICE_NAME = "sidss-http";
