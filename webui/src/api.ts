import * as store from "svelte/store";
import type { HttpEndpoint } from "sidss-common/httpApi"; 
import type { Runtype, Static } from "runtypes";
import { token } from "./stores";
import { API_BASE } from "./config";

export class ApiError extends Error {
    constructor(public status: number, public message: string) {
        super(`${status}: ${message}`);
    }
}

export async function callEndpoint<Req extends Runtype,
                                   Resp extends Runtype>(
        endpoint: HttpEndpoint<Req, Resp, boolean>,
        req: Static<Req>): Promise<Static<Resp>> {
    const headers = {};
    const tokenVal = store.get(token);
    if(tokenVal) {
        headers["Authorization"] = "BEARER " + tokenVal;
    }
    const resp = await fetch(API_BASE + "/" + endpoint.path, {
        method: "POST",
        headers,
        body: JSON.stringify(req),
    });
    const text = await resp.text();
    if(resp.status !== 200) {
        if(resp.status !== 403
           && (text === "Invalid token!" || text === "Expired token!")) {
            token.set(null);
        }
        throw new ApiError(resp.status, text);
    }
    if(text.length === 0) {
        return undefined;
    }
    return JSON.parse(text);
}
