import { Database, DbCollection } from "./db";
import { Group, GroupRuntype, Permission, PermissionRuntype, Service, ServiceRuntype, User, UserRuntype } from "./mgmtModel";
import { collectAsync } from "./utils";

export let db: Database;

export let dbServices: DbCollection<Service>;
export let dbPermissions: DbCollection<Permission>;
export let dbUsers: DbCollection<User>;
export let dbGroups: DbCollection<Group>;

export function initDb(newDb: Database) {
    db = newDb;
    dbServices = new DbCollection(db, "services", ServiceRuntype);
    dbPermissions = new DbCollection(db, "permissions", PermissionRuntype);
    dbUsers = new DbCollection(db, "users", UserRuntype);
    dbGroups = new DbCollection(db, "groups", GroupRuntype);
}

export async function userFullPermissions(user: User) {
    const permissionSets = [user.permissions];
    for(const groupId of user.memberOf) {
        const group = await dbGroups.getById(groupId);
        permissionSets.push(group.permissions);
    }
    if(permissionSets.includes("*")) {
        return new Set(
            (await collectAsync(dbPermissions.getAll()))
            .map(p => p[0]));
    }
    return new Set(permissionSets.flat());
}
