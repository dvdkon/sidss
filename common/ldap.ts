import * as argon2 from "argon2";

export interface EntryBindData {
    // List of services that are allowed to use this bind password
    // "*" means "don't check service"
    services: string[] | "*";
    hash: string;
    expiresAt?: number; // UNIX time
}

export interface LdapEntry {
    dn: string;
    attributes: Record<string, string | string[]>;
    bindPws?: EntryBindData[];
};

export function validatePassword(bindPws: EntryBindData[], service: string, password: string) {
    const hashPromises = [];
    const now = new Date().getTime() / 1000;
    for(const bindPw of bindPws || []) {
        const valid = bindPw.expiresAt === undefined || bindPw.expiresAt < now;
        if(valid && (bindPw.services === "*" || bindPw.services.includes(service))) {
            hashPromises.push((async () => {
                if(await argon2.verify(bindPw.hash, password)) {
                    return true;
                } else {
                    throw new Error("Password did not match!");
                }
            })());
        }
    }
    
    return Promise.any(hashPromises)
        .then(_ => { return true; },
              // No bindPws matched
              _ => { return false; });
}
