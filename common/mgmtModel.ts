import { Record, String, Number, Array, Static, Union, Literal, Optional } from "runtypes";

export const BindPwRuntype = Record({
    id: String, // Internal ID
    // List of service names
    // "*" for allowing all services
    services: Union(Literal("*"), Array(String)),
    hash: String, // Argon2 hash
    expiresAt: Optional(Number), // UNIX time
    description: Optional(String),
});
export type BindPw = Static<typeof BindPwRuntype>;

// Database ID - service name
export const ServiceRuntype = Record({
    description: Optional(String),
});
export type Service = Static<typeof ServiceRuntype>;

// Database ID - permission name
export const PermissionRuntype = Record({
    description: Optional(String),
});
export type Permission = Static<typeof PermissionRuntype>;

// Database ID - LDAP uid
export const UserRuntype = Record({
    uid: Optional(Number), // Also used for GID
    memberOf: Array(String), // Array of group DB IDs
    // Array pf permission DB IDs
    // "*" for all known permissions
    permissions: Union(Literal("*"), Array(String)),
    bindPws: Array(BindPwRuntype),
    // Email addresses that should be handled by a connected email server
    hostedEmails: Optional(Array(String)),
}).withConstraint(user => {
    if(user.bindPws) {
        const idSet = new Set(user.bindPws.map(bp => bp.id));
        return (idSet.size === user.bindPws.length)
            || "BindPw IDs not unique for user!";
    }
    return true;
});
export type User = Static<typeof UserRuntype>;

// Database ID - LDAP cn
export const GroupRuntype = Record({
    gid: Optional(Number), // Unix numeric GID
    // Array pf permission DB IDs
    // "*" for all known permissions
    permissions: Union(Literal("*"), Array(String)),
    description: Optional(String),
});
export type Group = Static<typeof GroupRuntype>;
