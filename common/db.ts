import * as path from "path";
import { promises as fsp } from "fs";
import { Runtype, ValidationError } from "runtypes";
import { unreachable } from "./utils";

export class InvalidIdError extends Error {
    constructor(public id: string) {
        super(`ID "${id}" is invalid`);
    }
}

export class EntryNotFoundError extends Error {
    constructor(public collection: string, public id: string) {
        super(`Entry "${id}" in "${collection}" not found`);
    }
}

export class EntryReadOnlyError extends Error {
    constructor(public collection: string, public id: string) {
        super(`Entry "${id}" in "${collection}" is read-only`);
    }
}

export class InvalidValueError extends Error {
    constructor(public collection: string, public id: string, public inner: Error) {
        super(`ID "${id}" in "${collection}" is invalid: ${inner.message}`);
    }
}

export class InvalidNewValueError extends Error {
    constructor(public collection: string, public id: string, public inner: Error) {
        super(`New entry of ID "${id}" for "${collection}" is invalid: ${inner.message}`);
    }
}

export function isValidId(id: string) {
    return id.match(/[a-zA-Z0-9._@-]+/);
}

export class Database {
    constructor(public mainDir: string, public roDirs: string[] = []) { }

    validateId(id: string) {
        if(!isValidId(id)) {
            throw new InvalidIdError(id);
        }
    }

    async tryGetById(collection: string, id: string) {
        this.validateId(id);
        for(const dir of [this.mainDir, ...this.roDirs]) {
            const filePath = path.join(dir, collection, id);
            try {
                return await fsp.readFile(filePath);
            } catch(e: any) {
                if(e?.code != "ENOENT") {
                    throw e;
                }
            }
        }
        return null;
    }

    async getById(collection: string, id: string) {
        const res = this.tryGetById(collection, id);
        if(res === null) {
            throw new EntryNotFoundError(collection, id);
        }
        return res;
    }

    async * getAll(collection: string): AsyncGenerator<[string, Buffer]> {
        for(const dir of [this.mainDir, ...this.roDirs]) {
            try {
                const colPath = path.join(dir, collection);
                const entries = await fsp.readdir(colPath);
                for(const entry of entries) {
                    const buf = await fsp.readFile(path.join(colPath, entry));
                    yield [entry, buf];
                }
            } catch(e: any) {
                if(e.code != "ENOENT") {
                    throw e;
                }
            }
        }
    }

    async save(collection: string, id: string, data: Buffer) {
        this.validateId(id);
        await fsp.mkdir(path.join(this.mainDir, collection), { recursive: true });
        const filePath = path.join(this.mainDir, collection, id);
        await fsp.writeFile(filePath, data);
    }

    async remove(collection: string, id: string) {
        this.validateId(id);
        const rwPath = path.join(this.mainDir, collection, id);
        try {
            return await fsp.unlink(rwPath);
        } catch(e: any) {
            if(e.code != "ENOENT") {
                throw e;
            }
        }

        for(const dir of this.roDirs) {
            try {
                await fsp.access(path.join(dir, collection, id));
            } catch(e: any) {
                if(e.code != "ENOENT") {
                    throw e;
                }
                throw new EntryReadOnlyError(collection, id);
            }
        }

        throw new EntryNotFoundError(collection, id);
    }
}

// Helper class for JSON files in Database
export class DbCollection<T> {
    constructor(
        public db: Database,
        public collection: string,
        public runtype?: Runtype) { }

    async tryGetById(id: string) {
        const buf = await this.db.getById(this.collection, id);
        try {
            if(buf === null) return null;
            const res = JSON.parse(buf.toString("utf8")) as T;
            this.runtype?.check(res);
            return res;
        } catch(e) {
            if(e instanceof ValidationError || e instanceof SyntaxError) {
                throw new InvalidValueError(this.collection, id, e);
            }
        }
        unreachable();
    }

    async getById(id: string) {
        const res = await this.tryGetById(id);
        if(res === null) {
            throw new EntryNotFoundError(this.collection, id);
        }
        return res;
    }

    async * getAll(): AsyncGenerator<[string, T]> {
        for await(const [id, buf] of this.db.getAll(this.collection)) {
            try {
                const res = JSON.parse(buf.toString("utf8")) as T;
                this.runtype?.check(res);
                yield [id, res];
            } catch(e) {
                if(e instanceof ValidationError || e instanceof SyntaxError) {
                    throw new InvalidValueError(this.collection, id, e);
                }
            }
        }
    }

    async save(id: string, val: T) {
        try {
            this.runtype?.check(val);
        } catch(e) {
            if(e instanceof ValidationError) {
                throw new InvalidNewValueError(this.collection, id, e);
            }
        }
        this.db.save(this.collection, id, Buffer.from(JSON.stringify(val), "utf8"));
    }

    async remove(id: string) {
        this.db.remove(this.collection, id);
    }
}
