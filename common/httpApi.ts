import { Array, Record, Runtype, String, Number, Undefined, Union, Literal, Optional, Static, Void } from "runtypes";
import { GroupRuntype, ServiceRuntype, UserRuntype } from "./mgmtModel";

export interface HttpEndpoint<Req extends Runtype,
                              Resp extends Runtype,
                              Auth extends boolean> {
    path: string,
    authenticated: Auth,
    request: Req,
    response: Resp,
}

export const login = {
    path: "login",
    authenticated: false as false,
    request: Record({
        username: String,
        password: String,
    }),
    response: Record({
        token: String,
    }),
};

export const getSelfUsername = {
    path: "getSelfUsername",
    authenticated: true as true,
    request: Undefined,
    response: String,
};

export const getSelfInfo = {
    path: "getSelfInfo",
    authenticated: true as true,
    request: Undefined,
    response: Record({
        id: String,
        user: UserRuntype,
        fullPermissions: Array(String),
    }),
};

export const getServices = {
    path: "getServices",
    authenticated: true as true,
    request: Undefined,
    response: Array(Record({
        id: String,
        service: ServiceRuntype,
    })),
}

export const getGroups = {
    path: "getGroups",
    authenticated: true as true,
    request: Undefined,
    response: Array(Record({
        id: String,
        group: GroupRuntype,
    })),
}

export const getUsers = {
    path: "getUsers",
    authenticated: true as true,
    request: Undefined,
    response: Array(Record({
        id: String,
        user: UserRuntype,
    })),
};

// Will keep bindPw hashes at original values if the new value is ""
export const upsertUser = {
    path: "upsertUser",
    authenticated: true as true,
    request: Record({
        id: String,
        user: UserRuntype,
    }),
    response: Undefined,
}
