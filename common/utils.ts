export async function collectAsync<T>(gen: AsyncGenerator<T>):
        Promise<T[]> {
    const arr = [];
    for await(const item of gen) {
        arr.push(item);
    }
    return arr;
}

export async function collectConcatAsync<T>(gen: AsyncGenerator<T[]>):
        Promise<T[]> {
    const arr = [];
    for await(const items of gen) {
        arr.push(...items);
    }
    return arr;
}

export class UnreachableError extends Error {}

export function unreachable(): never {
    throw new UnreachableError();
}

export function mapNull<T, R>(val: T | undefined | null, f: (v: T) => R): R | null {
    if(val === null || val === undefined) return null;
    return f(val);
}
