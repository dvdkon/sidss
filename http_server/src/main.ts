import * as http from "http";
import _ from "lodash";
import { Command } from "commander";
import { Runtype, Static } from "runtypes";
import { NotFound, Forbidden, MethodNotAllowed, BadRequest } from "http-errors";
import { Database, EntryNotFoundError, InvalidIdError, isValidId } from "sidss-common/db";
import { validatePassword } from "sidss-common/ldap";
import { initDb, dbUsers, dbServices, dbGroups, userFullPermissions } from "sidss-common/mgmtModelDb"
import { HttpEndpoint, getSelfInfo, login, getSelfUsername, getServices, getGroups, upsertUser, getUsers } from "sidss-common/httpApi"
import { ExpiredTokenError, newToken, userForToken } from "./auth";
import { initAuxiliaryDb } from "./auxDb";
import { collectAsync } from "../../common/utils";
import { readFile } from "fs/promises";

const cmd = new Command()
    .requiredOption("-b --bind <addr>", "HTTP bind address")
    .requiredOption("-p --port <num>", "HTTP bind port", parseInt)
    .requiredOption("-s --service <name>", "Service name", "sidss-http")
    .requiredOption("-a --aux-db <path>", "Auxiliary database directory")
    .requiredOption("-d --db <path>", "Primary database directory")
    .option("-P --mgmt-pidfile <path>", "PID file for mgmt_daemon")
    .option("-D --rodb <path...>", "Read-only database directories")
    .option("-V --token-validity <ms>", "Token validity length", parseInt, 600000);
cmd.parse(process.argv);
const opts = cmd.opts();

const adminPermission = opts.service + ".admin";
const selfChangeBindPwsPermission = opts.service + ".self-change-bindpws";

initDb(new Database(opts.db, opts.rodb));
initAuxiliaryDb(new Database(opts.auxDb));

interface EndpointHandler {
    endpoint: HttpEndpoint<any, any, boolean>,
    handler: (username: string /* | null */, req: any) => Promise<any>,
}

const endpointHandlers: Record<string, EndpointHandler> = {};
function endpointHandler<Req extends Runtype,
                         Resp extends Runtype,
                         Auth extends boolean>(
        endpoint: HttpEndpoint<Req, Resp, Auth>,
        handler: (username: Auth extends true ? string : string | null,
                  req: Static<Req>)
                 => Promise<Static<Resp>>) {
    endpointHandlers[endpoint.path] = {endpoint, handler};
}

async function refreshLdap() {
    if(opts.mgmtPidfile) {
        const pid = parseInt(await readFile(opts.mgmtPidfile, "utf-8"));
        process.kill(pid, "SIGUSR1");
    }
}

endpointHandler(login, async (_username, req) => {
    let user;
    try {
        user = await dbUsers.getById(req.username);
    } catch(e) {
        if(e instanceof EntryNotFoundError || e instanceof InvalidIdError) {
            throw new Forbidden("Invalid user!");
        } else {
            throw e;
        }
    }
    if(!await validatePassword(user.bindPws || [], opts.service, req.password)) {
        throw new Forbidden("Invalid password!");
    }

    return {
        token: await newToken(req.username, opts.tokenValidity),
    };
});

endpointHandler(getSelfUsername, async (username) => {
    try {
        return username;
    } catch(e) {
        if(e instanceof EntryNotFoundError) {
            throw new Forbidden("Invalid token!");
        } else {
            throw e;
        }
    }
});


endpointHandler(getSelfInfo, async (username) => {
    const user = await dbUsers.getById(username);
    for(const bindPw of user.bindPws || []) {
        bindPw.hash = "";
    }
    const fullPermissions = await userFullPermissions(user);
    return {
        id: username,
        user,
        fullPermissions: Array.from(fullPermissions),
    };
});

endpointHandler(getServices, async (_username) => {
    const services = [];
    for await(const [id, service] of dbServices.getAll()) {
        services.push({id, service});
    }
    return services;
});

endpointHandler(getGroups, async (_username) => {
    const groups = [];
    for await(const [id, group] of dbGroups.getAll()) {
        groups.push({id, group});
    }
    return groups;
});

endpointHandler(upsertUser, async (username, {id: changingId, user: userNew}) => {
    if(!isValidId(changingId)) {
        throw new BadRequest("Invalid ID");
    }

    const self = await dbUsers.getById(username);
    const userOld = await dbUsers.tryGetById(changingId);
    const fullPermissions = await userFullPermissions(self);

    for(const bindPw of userNew.bindPws) {
        if(bindPw.hash === "" && userOld) {
            const oldBindPw = userOld.bindPws.find(bp => bp.id === bindPw.id);
            if(oldBindPw) {
                bindPw.hash = oldBindPw.hash;
            }
        }
    }
    
    if(!fullPermissions.has(adminPermission)) {
        if(username !== changingId) {
            throw new Forbidden(
                "Need to have admin permission to change another user");
        }

        if(self.uid !== userNew.uid) {
            throw new Forbidden(
                "Need to have admin permission to change user UID");
        }
        if(!_.isEqual(self.permissions, userNew.permissions)) {
            throw new Forbidden(
                "Need to have admin permission to change user permissions");
        }
        if(!_.isEqual(self.memberOf, userNew.memberOf)) {
            throw new Forbidden(
                "Need to have admin permission to change user group membership");
        }
        if(!_.isEqual(self.hostedEmails, userNew.hostedEmails)) {
            throw new Forbidden(
                "Need to have admin permission to change hosted email list");
        }

        if(!fullPermissions.has(selfChangeBindPwsPermission)
           && !_.isEqual(self.bindPws, userNew.bindPws)) {
            throw new Forbidden(
                "You don't have permission to change your passwords");
        }
    }

    dbUsers.save(changingId, userNew);
    await refreshLdap();
    return undefined;
});

endpointHandler(getUsers, async (username) => {
    const self = await dbUsers.getById(username);
    const fullPermissions = await userFullPermissions(self);
    if(!fullPermissions.has(adminPermission)) {
        throw new Forbidden("Need to have admin permission to list all users");
    }
    const users = await collectAsync(dbUsers.getAll());
    return users.map(([id, user]) => ({id, user}));
});

http.createServer((req, res) => {
    let body = "";
    req.on("data", chunk => {
        body += chunk;
    });

    req.on("end", async () => {
        try {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Authorization");

            if(req.method === "OPTIONS") {
                res.writeHead(200);
                res.end();
                return;
            } else if(req.method! !== "POST") {
                throw new MethodNotAllowed();
            }

            let username: string | null = null;
            const authHeader = req.headers.authorization;
            if(authHeader) {
                if(!authHeader.startsWith("BEARER ")) {
                    throw new Forbidden();
                }
                const token = authHeader.replace(/^BEARER /, "");
                try {
                    username = await userForToken(token);
                } catch(e) {
                    if(e instanceof EntryNotFoundError) {
                        throw new Forbidden("Invalid token!");
                    } else if(e instanceof ExpiredTokenError) {
                        throw new Forbidden("Expired token!");
                    } else {
                        throw e;
                    }
                }
            }

            const handler = endpointHandlers[req.url!.replace(/^\//, "").replace(/\/$/, "")];
            if(handler === undefined) {
                throw new NotFound();
            }
            if(handler.endpoint.authenticated && username === null) {
                throw new Forbidden();
            }

            let reqObj;
            try {
                let reqJson;
                if(body === "") {
                    reqJson = undefined;
                } else {
                    reqJson = JSON.parse(body);
                }
                reqObj = handler.endpoint.request.check(reqJson);
            } catch(e) {
                console.log(e);
                throw new BadRequest();
            }

            // We cast away the | null because it'd be too hard to store the
            // handler in accordance with the endpoints
            const respJson = await handler.handler(username as string, reqObj);

            res.writeHead(200);
            res.end(JSON.stringify(respJson));
        } catch(e: any) {
            if(e.status !== undefined) {
                res.writeHead(e.status);
                res.end(e.message);
            } else {
                console.error("Error handling request:", e);
                res.writeHead(500);
                res.end();
            }
        }
    });
}).listen(opts.port, opts.bind);
