import * as crypto from "crypto";
import { promisify } from "util";
import { dbTokens } from "./auxDb";

export class ExpiredTokenError extends Error {
    constructor() {
        super("Tried to get user for expired token!");
    }
}

async function cleanTokens() {
    const now = new Date().getTime();
    for await(const [token, tokenData] of dbTokens.getAll()) {
        if(tokenData.expiresAt <= now) {
            dbTokens.remove(token);
        }
    }
}

const randomBytes = promisify(crypto.randomBytes);
export async function newToken(username: string, validForMs: number) {
    cleanTokens();
    const token = (await randomBytes(15)).toString("base64url");
    await dbTokens.save(token, {
        username,
        expiresAt: new Date().getTime() + validForMs,
    });
    return token;
}

export async function userForToken(token: string) {
    const {username, expiresAt} = await dbTokens.getById(token);
    if(expiresAt <= new Date().getTime()) {
        throw new ExpiredTokenError();
    }
    return username;
}
