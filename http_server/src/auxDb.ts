import { Static, Record, String, Number } from "runtypes";
import { DbCollection, Database } from "sidss-common/db";

export const TokenRuntype = Record({
    username: String,
    expiresAt: Number, // UNIX time in milliseconds 
});
type Token = Static<typeof TokenRuntype>;
export let dbTokens: DbCollection<Token>;
export function initAuxiliaryDb(db: Database) {
    dbTokens = new DbCollection(db, "tokens", TokenRuntype);
}
