This is a modified version of type definitions for ldapjs from
https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/ldapjs.

It includes more comprehensove types for the server-side functionality of
ldapjs.
