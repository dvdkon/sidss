import { dbGroups, dbUsers, userFullPermissions } from "sidss-common/mgmtModelDb";
import { LdapEntry } from "sidss-common/ldap";

export async function* genUsers(baseDn: string): AsyncGenerator<LdapEntry[]> {
    for await(const [id, user] of dbUsers.getAll()) {
        const attrs: Record<string, string | string[]> = {
            cn: id,
            uid: id,
            homeDirectory: `/ldaphome/${id}`,
            loginShell: "/bin/sh",
            permissions: Array.from(await userFullPermissions(user)),
            memberOf: user.memberOf.map(g => `cn=${g},ou=groups,${baseDn}`),
            hostedEmails: user.hostedEmails || [],
        };
        if(user.uid !== undefined) {
            attrs.objectClass = "posixAccount";
            attrs.uidNumber = user.uid.toString();
            attrs.gidNumber = user.uid.toString();
        } else {
            attrs.objectClass = "account";
        }
        yield [
            // See RFC 2307
            {
                dn: `uid=${id},ou=users,${baseDn}`,
                attributes: attrs,
                bindPws: user.bindPws,
            }
        ];
    }
}

export async function* genGroups(baseDn: string): AsyncGenerator<LdapEntry[]> {
    const memberships: Record<string, string []> = {};
    for await(const [userId, user] of dbUsers.getAll()) {
        for(const groupId of user.memberOf) {
            if(memberships[groupId]) {
                memberships[groupId].push(userId);
            } else {
                memberships[groupId] = [userId];
            }
        }
    }
    for await(const [id, group] of dbGroups.getAll()) {
        const attrs: Record<string, string | string[]> = {
            cn: id,
            permissions: group.permissions,
            memberUid: memberships[id],
            member: (memberships[id] || []).map(id => `uid=${id},ou=users,${baseDn}`),
        };
        if(group.gid !== undefined) {
            attrs.objectClass = "posixGroup";
            attrs.gidNumber = group.gid.toString();
        } else {
            attrs.objectClass ="groupOfNames";
        }
        yield [{
            dn: `cn=${id},ou=groups,${baseDn}`,
            attributes: attrs,
        }];
    }
    for await(const [id, user] of dbUsers.getAll()) {
        if(user.uid == undefined) continue;
        yield [{
            dn: `cn=${id},ou=groups,${baseDn}`,
            attributes: {
                objectClass: "posixGroup",
                cn: id,
                gidNumber: user.uid.toString(),
                memberUid: id,
                member: `uid=${id},ou=users,${baseDn}`,
            },
        }];
    }
}
