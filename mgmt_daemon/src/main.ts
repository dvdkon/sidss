import { Command, OptionValues } from "commander";
import * as shlex from "shlex";
import { Database } from "sidss-common/db";
import { initDb } from "sidss-common/mgmtModelDb";
import { collectConcatAsync } from "sidss-common/utils";
import { genUsers, genGroups } from "./ldapgen";
import { start, sendEntries } from "./ldapServer";

async function generateAndSendEntries(baseDn: string) {
    const users = await collectConcatAsync(genUsers(baseDn));
    const groups = await collectConcatAsync(genGroups(baseDn));
    const entries = users.concat(groups);
    sendEntries(entries);
    console.log("Generated and applied LDAP entries!");
}

async function main(opts: OptionValues) {
    initDb(new Database(opts.db, opts.rodb));
    start(shlex.split(opts.srvr));
    generateAndSendEntries(opts.base);

    process.on("SIGUSR1", async () => {
        // User-initiated LDAP regeneration
        try {
            await generateAndSendEntries(opts.base);
        } catch(e) {
            console.error("Error during user-initiated LDAP regeneration:\n", e);
        }
    });
}

const cmd = new Command()
    .requiredOption("-s --srvr <cmd>", "LDAP server command")
    .requiredOption("-d --db <path>", "Primary database directory")
    .requiredOption("-b --base <dn>", "Base DN, parent of all entries")
    .option("-D --rodb <path...>", "Read-only database directories");
cmd.parse(process.argv);
main(cmd.opts());
