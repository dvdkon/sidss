import * as childProcess from "child_process";
import { LdapEntry } from "sidss-common/ldap";

export class LdapServerNotStarted extends Error {
    constructor() {
        super("LDAP server not started!");
    }
}

const restartDelayMs = 5000;
let ldapServerProc: childProcess.ChildProcess;


export function start(cmd: string[]) {
    console.log("Starting LDAP server...");
    ldapServerProc = childProcess.spawn(cmd[0], cmd.slice(1));
    ldapServerProc.stdout!!.on("data", (data) => {
        console.log("LDAP server stdout: %s", data.toString().trim());
    });
    ldapServerProc.stderr!!.on("data", (data) => {
        console.log("LDAP server stderr: %s", data.toString().trim());
    });
    ldapServerProc.on("exit", (code) => {
        console.warn("LDAP server exited with %d! Restarting after %dms...",
                     code, restartDelayMs);
        setTimeout(() => { start(cmd); }, restartDelayMs);
    });

    function gracefulExit() {
        console.log("Killing LDAP server...");
        ldapServerProc.kill();
        process.exit(0);
    }

    process.on("SIGTERM", gracefulExit);
    process.on("SIGINT", gracefulExit);
}

export function sendEntries(entries: LdapEntry[]) {
    if(!ldapServerProc) throw new LdapServerNotStarted();
    // This relies on JSON.stringify not using newlines
    ldapServerProc.stdin!!.write(JSON.stringify(entries) + "\n");
}
