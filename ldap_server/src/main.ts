import * as readline from "readline";
import * as ldap from "ldapjs";
import { Command } from "commander";
import { LdapEntry, EntryBindData, validatePassword } from "../../common/ldap";
import { mapNull } from "../../common/utils";

type Attributes = Record<string, string | string[]>;

interface LdapTreeEntry {
    rdn: ldap.RDN,
    attributes: Attributes,
    bindPws?: EntryBindData[],
    children?: LdapTreeEntry[],
    hierHelper: boolean; // Exists only as a hierarchy helper, to hold children
}

interface PGM {
    permission: string,
    groupName: string,
    gid: number,
}

function entryAddChild(entry: LdapTreeEntry, child: LdapTreeEntry) {
    if(!entry.children) {
        entry.children = [];
    }
    entry.children.push(child);
}

function attrHas(attr: string | string[], val: string) {
    return typeof attr == "object" ? val in attr : val == attr;
}

function copyAttrs(attrs: Attributes) {
    return Object.fromEntries(
        Object.entries(attrs).map(a =>
            [a[0], typeof a[1] === "string" ? a[1] : [...a[1]]]));
}

function findByDn(root: LdapTreeEntry, dn: ldap.DN | string, create: boolean) {
    if(typeof dn == "string") {
        dn = ldap.parseDN(dn);
    }
    let current = root;
    const rdnsRev = [ ...dn.rdns ].reverse();
    for(const rdn of rdnsRev) {
        let next = current.children?.find(c => c.rdn.equals(rdn));
        if(!next) {
            if(create) {
                next = { rdn: rdn, attributes: {}, hierHelper: true };
                entryAddChild(current, next);
            } else {
                return null;
            }
        }
        current = next;
    }
    return current;
}

function splitSpecialFromDn(dn: ldap.DN, specialAttr: string): [ldap.DN, string[]] {
    const special = [];
    const rdns = [];
    for(const rdn of dn.rdns) {
        let userRdnAttrs: Record<string, string> | null = null;
        for(const attr of Object.values(rdn.attrs)) {
            if(attr.name === specialAttr) {
                special.push(attr.value);
            } else {
                userRdnAttrs ??= {};
                userRdnAttrs[attr.name] = attr.value;
            }
        }
        if(userRdnAttrs != null) {
            rdns.push(new ldap.RDN(userRdnAttrs));
        }
    }
    return [new ldap.DN(rdns), special];
}

// PGMs - Permission-Group mappings
function splitPgmsFromDn(dn: ldap.DN) {
    const [splitDn, pgmsText] = splitSpecialFromDn(dn, "pgm");
    const pgms = pgmsText.map(t => {
            const s = t.split(";");
            if(s.length != 3) return null;
            const gid = parseInt(s[2]);
            if(isNaN(gid)) return null;
            return {permission: s[0], groupName: s[1], gid};
        }).filter(pgm => pgm != null);
    return [splitDn, pgms] as [ldap.DN, PGM[]];
}

function splitServiceFromDn(dn: ldap.DN): [ldap.DN, string] {
    const [rest, services] = splitSpecialFromDn(dn, "svc");
    const service = services.length == 0 ? "" : services[0];
    return [rest, service];
}

function getPgmDn(baseDn: ldap.DN, pgm: PGM): ldap.DN {
    const rdn = new ldap.RDN({cn: pgm.groupName});
    return new ldap.DN([rdn, new ldap.RDN({ou: "pgms"}), ...baseDn.rdns]);
}

function getPgmAttrs(baseDn: ldap.DN, pgm: PGM): Attributes {
    // TODO: Precompute PGMs and minimally "template in" the name and UID?
    // This is more DB logic than I wanted in the LDAP server...
    const memberUid: string[] = [];
    const member: string[] = [];
    function findMembersRec(rdns: ldap.RDN[], entry: LdapTreeEntry) {
        if(attrHas(entry.attributes.objectClass, "posixAccount")
           && entry.attributes.permissions.includes(pgm.permission)) {
            memberUid.push(entry.attributes.uid as string);
            member.push(new ldap.DN([entry.rdn, ...rdns])
                        .format({skipSpace: true}));
        }
        for(const child of entry.children || []) {
            findMembersRec([entry.rdn, ...rdns], child);
        }
    }
    findMembersRec(baseDn.rdns, ldapRoot);

    return {
        cn: pgm.groupName,
        objectClass: "posixGroup",
        gidNumber: pgm.gid.toString(),
        memberUid,
        member,
    };
}

const cmd = new Command()
    .option("-p --bind-port <port>", "LDAP bind TCP port")
    .option("-a --bind-addr <addr>", "LDAP bind IP address");
cmd.parse(process.argv);
const opts = cmd.opts();

let ldapRoot: LdapTreeEntry = {
    rdn: new ldap.RDN({}),
    attributes: {},
    hierHelper: true
};

const stdinReadline = readline.createInterface({
    input: process.stdin,
    terminal: false,
});
stdinReadline.on("line", (line) => {
    const ldapEntries = JSON.parse(line) as LdapEntry[];
    const newRoot: LdapTreeEntry = {
        rdn: new ldap.RDN({}),
        attributes: {},
        hierHelper: true
    };
    for(const entry of ldapEntries) {
        const treeEntry = findByDn(newRoot, entry.dn, true)!!;
        treeEntry.hierHelper = false;
        treeEntry.attributes = entry.attributes;
        treeEntry.bindPws = entry.bindPws;
    }
    ldapRoot = newRoot;
});

const server = ldap.createServer();

function hasSearchPermissions(dn: ldap.DN, bindDn?: ldap.DN) {
    if(!bindDn) {
        return false;
    }

    const caller = findByDn(ldapRoot, bindDn, false);
    const callerPerms = caller?.attributes?.permissions as string[] || [];
    if(!callerPerms.includes("sidss-ldap.search-all")) {
        if(!callerPerms.includes("sidss-ldap.search-self")
           || !dn.equals(bindDn)) {
            return false;
        }
    }
    return true;
}

server.search("", (req, res, next) => {
    const bindDn = mapNull(
        req.connection.ldap.bindDN,
        dn => splitServiceFromDn(dn)[0]);
    // svc= RDNs aren't used on the base DN, but some services will send them
    // anyway, since the user can't set different bind and user DNs
    // We need to send them back entries with the same svc= value, so that they
    // will then bind the user correctly.
    const [dn2, service] = splitServiceFromDn(req.dn);
    const [baseDn, pgms] = splitPgmsFromDn(dn2);

    if(!hasSearchPermissions(baseDn, bindDn ?? undefined)) {
        return next(new ldap.InsufficientAccessRightsError());
    }

    function send(dn: ldap.DN, entry: LdapTreeEntry) {
        let attributes = entry.attributes;
        if(entry.hierHelper || !req.filter.matches(attributes)) return;
        if(attrHas(attributes.objectClass, "posixAccount")) {
            attributes = copyAttrs(attributes);
            attributes.memberOf ??= [];
            for(const pgm of pgms) {
                if((attributes.permissions as string[]).includes(pgm.permission)) {
                    const dn = getPgmDn(baseDn, pgm).format({skipSpace: true});
                    (attributes.memberOf as string[]).push(dn);
                }
            }
            dn = new ldap.DN([new ldap.RDN({svc: service}), ...dn.rdns]);
        }
        res.send({ dn: dn.format({skipSpace: true}), attributes });
    }

    const base = findByDn(ldapRoot, baseDn, false);
    if(!base) {
        return next(new ldap.NoSuchObjectError());
    }

    if(req.scope == "base") {
        send(baseDn, base);
    } else if(req.scope == "one") {
        for(const child of base.children || []) {
            send(new ldap.DN([child.rdn, ...baseDn.rdns]), child);
        }
    } else if(req.scope == "sub") {
        function sendRec(entry: LdapTreeEntry, entryDn: ldap.DN) {
            send(entryDn, entry);

            for(const child of entry.children || []) {
                const childDn = new ldap.DN([child.rdn, ...entryDn.rdns]);
                sendRec(child, childDn);
            }
        }

        sendRec(base, baseDn);
    }

    for(const pgm of pgms) {
        const dn = getPgmDn(baseDn, pgm);
        const attributes = getPgmAttrs(baseDn, pgm);
        if(req.filter.matches(attributes)) {
            res.send({dn: dn.format({skipSpace: true}), attributes});
        }
    }


    res.end();
});

server.bind("", (req, res, next) => {
    if(req.authentication !== "simple") {
        return next(new ldap.AuthMethodNotSupportedError());
    }

    const [userDn, service] = splitServiceFromDn(req.dn);

    const entry = findByDn(ldapRoot, userDn, false);
    if(!entry || entry.hierHelper) {
        return next(new ldap.NoSuchObjectError());
    }

    validatePassword(entry.bindPws || [], service, req.credentials)
        .then(success => {
            if(success
               && (service == ""
                   || entry.attributes.permissions.includes(service))) {
                res.end();
                return next();
            } else {
                next(new ldap.InvalidCredentialsError());
            }
        });
});

server.listen(
    opts.bindPort || 389,
    opts.bindAddr || "0.0.0.0",
    () => {
        console.log('LDAP server listening at', server.url);
    });
